package model

import "time"

type UserToken struct {
	ID           uint      `json:"id"`
	UserID       uint      `json:"user_id"`
	Token        string    `json:"token"`
	ExpiredAt    time.Time `json:"expired_at"`
	IsActiveFlag bool      `json:"is_active_flag"`
	CreatedAt    time.Time `json:"created_at"`
	UpdatedAt    time.Time `json:"updated_at"`
}
