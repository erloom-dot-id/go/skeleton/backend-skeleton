package response

import "backend-skeleton/model"

type GetUserResponse struct {
	BasePaginateResponse
	Users []model.User `json:"users"`
}
