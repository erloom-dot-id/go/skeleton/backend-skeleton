package response

import "backend-skeleton/model"

type LoginResponse struct {
	AccessToken string     `json:"access_token"`
	User        model.User `json:"user"`
}
