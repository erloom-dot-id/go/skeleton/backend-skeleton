package config

import (
	"context"
	"fmt"
	"os"
	"strconv"

	"gitlab.com/erloom-dot-id/go/echo-go-logger/elk"
	"gitlab.com/erloom-dot-id/go/echo-go-logger/sentry"
)

func NewSentry() {
	traceSampleRate, _ := strconv.ParseFloat(os.Getenv("SENTRY_TRACE_SAMPLE_RATE"), 64)

	sentryClient := sentry.SentryClient{}

	sentryClient.Dsn = os.Getenv("SENTRY_DSN")
	sentryClient.TracesSampleRate = traceSampleRate
	sentryClient.EnableTracing = true
	sentryClient.Environment = os.Getenv("ENV")

	err := sentry.InitNewSentry(sentryClient)
	if err != nil {
		elk.LogError(context.Background(), fmt.Sprintf("Sentry init err : %v \n", err))
	}
}
