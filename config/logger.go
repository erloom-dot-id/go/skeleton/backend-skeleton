package config

import (
	"os"

	"gitlab.com/erloom-dot-id/go/echo-go-logger/elk"
)

func NewLogger() {
	var setup = elk.ElkSetup{
		Path:          os.Getenv("LOG_PATH"),
		ForceLocalLog: os.Getenv("FORCE_LOCAL_LOG"),
	}
	elk.Init(setup)
}
