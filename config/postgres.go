package config

import (
	"backend-skeleton/lib"
	"context"
	"fmt"
	"os"

	"gitlab.com/erloom-dot-id/go/echo-go-logger/elk"
	logger2 "gorm.io/gorm/logger"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func NewPG() (*lib.Database, error) {
	username := os.Getenv("POSTGRES_USERNAME")
	password := os.Getenv("POSTGRES_PASSWORD")
	host := os.Getenv("POSTGRES_HOST")
	port := os.Getenv("POSTGRES_PORT")
	dbname := os.Getenv("POSTGRES_DATABASE")

	dsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s sslmode=disable TimeZone=Asia/Jakarta", host, username, password, dbname, port)
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{
		Logger: logger2.Default.LogMode(logger2.Silent),
	})
	if err != nil {
		return nil, err
	}

	elk.LogInfo(context.Background(), "successfully connected to postgres")
	return &lib.Database{DB: db}, nil
}
