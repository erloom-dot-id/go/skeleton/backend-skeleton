package storage

import (
	"context"
	"errors"
	"fmt"
	"io"
	"os"
	"path/filepath"

	"gitlab.com/erloom-dot-id/go/echo-go-logger/elk"
	"go.uber.org/zap"
)

type Local struct {
	Directory string // relative public
}

func (m *Local) UploadFile(ctx context.Context, bucketName, fileName, contentType string, file io.Reader) error {
	path := filepath.Join(m.Directory, bucketName, fileName)
	os.MkdirAll(filepath.Dir(path), os.ModePerm)

	f, err := os.OpenFile(path, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0666)
	if err != nil {
		elk.LogError(ctx, "failed open local file", []zap.Field{
			zap.Error(err),
			zap.Strings("tags", []string{"storage", "local"}),
		}...)

		return err
	}

	defer f.Close()
	_, err = io.Copy(f, file)
	if err != nil {
		elk.LogError(ctx, "failed copy local file", []zap.Field{
			zap.Error(err),
			zap.Strings("tags", []string{"storage", "local"}),
		}...)

		return err
	}

	if fileInfo, err := os.Stat(path); fileInfo.Size() > 10000000 {
		if err != nil {
			return errors.New("file not found")
		}

		return errors.New("file too large")
	}

	return nil
}

func (m *Local) GetFileTemporaryURL(ctx context.Context, bucketName, filename string) (string, error) {
	return fmt.Sprintf("%s/%s", os.Getenv("CDN_BASE_URL"), filename), nil
}

func (m *Local) GetObject(_ context.Context, _, filename string) (io.Reader, error) {
	return os.Open(fmt.Sprintf("%s", filename))
}

func (m *Local) FGetObject(ctx context.Context, bucketName, filename, destination string) error {
	return nil
}

func (m *Local) FPutObject(ctx context.Context, bucketName, filename, source string) error {
	return nil
}

func (m *Local) RemoveFile(ctx context.Context, bucketName, filename string) error {
	return nil
}
