package usecase

import (
	"backend-skeleton/model"
	"backend-skeleton/request"
	"context"
	"errors"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

var userExample = model.User{
	ID:       1,
	Username: "username",
	Email:    "username@mail.id",
}

func TestCreateUser(t *testing.T) {
	ctx := context.Background()
	reqExample := request.CreateUserRequest{
		Username: "username",
		Email:    "username@mail.id",
	}

	Convey("CreateUser Usecase Scenario", t, func() {
		Convey("success", func() {
			r := m.On("CreateUser", mock.Anything).Return(userExample, nil)

			res, err := u.CreateUser(ctx, reqExample)

			So(res, ShouldNotBeNil)
			So(err, ShouldBeNil)

			r.Unset()
		})
		Convey("fail", func() {
			r := m.On("CreateUser", mock.Anything).Return(userExample, errors.New("Error CreateUser"))

			res, err := u.CreateUser(ctx, reqExample)

			So(res, ShouldBeNil)
			So(err, ShouldNotBeNil)

			r.Unset()
		})
	})
}

func TestGetUsers(t *testing.T) {
	ctx := context.Background()

	query := request.GetUserQuery{}
	query.Page = 1
	query.Limit = 10
	users := []model.User{
		userExample,
	}
	total := uint(1)

	Convey("GetUsers Usecase Scenario", t, func() {
		Convey("all success", func() {
			r1 := m.On("GetUsers", query).Return(users, nil)
			r2 := m.On("GetUserTotal", query).Return(total, nil)

			res, err := u.GetUsers(ctx, query)
			So(res, ShouldNotBeNil)
			So(err, ShouldBeNil)

			r1.Unset()
			r2.Unset()
		})

		Convey("GetUsers: success, GetUserTotal: failed", func() {
			r1 := m.On("GetUsers", query).Return(users, nil)
			r2 := m.On("GetUserTotal", query).Return(nil, errors.New("Error GetUserTotal"))

			res, err := u.GetUsers(ctx, query)
			So(res, ShouldBeNil)
			So(err, ShouldNotBeNil)

			r1.Unset()
			r2.Unset()
		})

		Convey("GetUsers: failed, GetUserTotal: success", func() {
			r1 := m.On("GetUsers", query).Return(nil, errors.New("Error GetUsers"))
			r2 := m.On("GetUserTotal", query).Return(total, nil)

			res, err := u.GetUsers(ctx, query)
			So(res, ShouldBeNil)
			So(err, ShouldNotBeNil)

			r1.Unset()
			r2.Unset()
		})

		Convey("all: failed", func() {
			r1 := m.On("GetUsers", query).Return(nil, errors.New("Error GetUsers"))
			r2 := m.On("GetUserTotal", query).Return(nil, errors.New("Error GetUserTotal"))

			res, err := u.GetUsers(ctx, query)
			So(res, ShouldBeNil)
			So(err, ShouldNotBeNil)

			r1.Unset()
			r2.Unset()
		})
	})
}

func TestUpdateUser(t *testing.T) {
	ctx := context.Background()

	Convey("UpdateUser Usecase Scenario", t, func() {
		Convey("all: success", func() {
			r1 := m.On("GetUserByID", mock.Anything).Return(userExample, nil)
			r2 := m.On("UpdateUser", mock.Anything).Return(userExample, nil)

			res, err := u.UpdateUser(ctx, request.UpdateUserRequest{UserID: 1})

			So(res, ShouldNotBeNil)
			So(err, ShouldBeNil)

			r1.Unset()
			r2.Unset()
		})
		Convey("GetUserByID: success, UpdateUser: failed", func() {
			r1 := m.On("GetUserByID", mock.Anything).Return(userExample, nil)
			r2 := m.On("UpdateUser", mock.Anything).Return(nil, errors.New("Error UpdateUser"))

			res, err := u.UpdateUser(ctx, request.UpdateUserRequest{UserID: 1})

			So(res, ShouldBeNil)
			So(err, ShouldNotBeNil)

			r1.Unset()
			r2.Unset()
		})
		Convey("GetUserByID: failed, UpdateUser: success", func() {
			r1 := m.On("GetUserByID", mock.Anything).Return(nil, errors.New("Error GetUserByID"))
			r2 := m.On("UpdateUser", mock.Anything).Return(userExample, nil)

			res, err := u.UpdateUser(ctx, request.UpdateUserRequest{UserID: 1})

			So(res, ShouldBeNil)
			So(err, ShouldNotBeNil)

			r1.Unset()
			r2.Unset()
		})
		Convey("all: failed", func() {
			r1 := m.On("GetUserByID", mock.Anything).Return(nil, errors.New("Error GetUserByID"))
			r2 := m.On("UpdateUser", mock.Anything).Return(nil, errors.New("Error UpdateUser"))

			res, err := u.UpdateUser(ctx, request.UpdateUserRequest{UserID: 1})

			So(res, ShouldBeNil)
			So(err, ShouldNotBeNil)

			r1.Unset()
			r2.Unset()
		})
	})
}

func TestGetUserByID(t *testing.T) {
	ctx := context.Background()

	Convey("GetUserDetail Usecase Scenario", t, func() {
		Convey("success", func() {
			r := m.On("GetUserByID", mock.Anything).Return(userExample, nil)

			res, err := u.GetUserDetail(ctx, uint(1))

			So(res, ShouldNotBeNil)
			So(err, ShouldBeNil)

			r.Unset()
		})
		Convey("fail", func() {
			r := m.On("GetUserByID", uint(0)).Return(nil, errors.New("Error GetUserByID"))

			res, err := u.GetUserDetail(ctx, uint(1))

			So(res, ShouldBeNil)
			So(err, ShouldNotBeNil)

			r.Unset()
		})
	})
}

func TestDeleteUser(t *testing.T) {
	ctx := context.Background()

	Convey("DeleteUser Usecase Scenario", t, func() {
		Convey("success", func() {
			r := m.On("DeleteUser", mock.Anything).Return(nil)

			err := u.DeleteUser(ctx, uint(1))

			So(err, ShouldBeNil)

			r.Unset()
		})
		Convey("fail", func() {
			r := m.On("DeleteUser", mock.Anything).Return(errors.New("Error DeleteUser"))

			err := u.DeleteUser(ctx, uint(1))

			So(err, ShouldNotBeNil)

			r.Unset()
		})
	})
}
