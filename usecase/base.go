package usecase

import (
	"backend-skeleton/config"
	interfacerepo "backend-skeleton/repository/interface-repo"
	mockrepo "backend-skeleton/repository/mock"
	"backend-skeleton/storage"
)

type Usecase struct {
	repo    interfacerepo.BaseRepository
	storage storage.Storage
}

func NewUsecase(repo interfacerepo.BaseRepository, storage storage.Storage) Usecase {
	return Usecase{repo: repo, storage: storage}
}

// VARIABLE FOR CALL ALL USECASE FUNCTION FOR UNIT TEST IN *_test.go
var m = &mockrepo.MockRepository{}
var u = NewUsecase(m, config.NewLocalStorage())
