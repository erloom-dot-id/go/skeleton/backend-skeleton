package usecase

import (
	"backend-skeleton/lib"
	"backend-skeleton/model"
	"backend-skeleton/request"
	"backend-skeleton/response"
	"context"
	"time"

	"github.com/google/uuid"
	"gitlab.com/erloom-dot-id/go/echo-go-logger/elk"
	"gitlab.com/erloom-dot-id/go/echo-go-logger/sentry"
	"go.uber.org/zap"
)

func (usecase *Usecase) Login(ctx context.Context, payload request.LoginRequest) (*response.LoginResponse, error) {
	ctx, span := sentry.StartSpan(ctx, "usecase.Login")
	defer span.Finish()

	user, err := usecase.repo.GetUserByUsername(ctx, payload.Username)
	if err != nil {
		return nil, err
	}

	err = lib.CompareHashAndPassword(user.EncryptedPassword, payload.Password)
	if err != nil {
		elk.LogError(ctx, "Error CompareHashAndPassword", []zap.Field{
			zap.Error(err),
			zap.Strings("tags", []string{"usecase", "login"}),
		}...)

		return nil, lib.ErrorWrongPassword
	}

	token := uuid.NewString()
	expiredAt := time.Now().Add(72 * time.Hour)
	userToken := model.UserToken{
		UserID:       user.ID,
		Token:        token,
		ExpiredAt:    expiredAt,
		IsActiveFlag: true,
		CreatedAt:    time.Now(),
		UpdatedAt:    time.Now(),
	}

	userToken, err = usecase.repo.CreateUserToken(ctx, userToken)
	if err != nil {
		return nil, err
	}

	res := response.LoginResponse{
		AccessToken: userToken.Token,
		User:        user,
	}

	return &res, nil
}
