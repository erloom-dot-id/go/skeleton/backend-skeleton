package usecase

import (
	"backend-skeleton/lib"
	"backend-skeleton/model"
	"backend-skeleton/request"
	"backend-skeleton/response"
	"context"
	"time"

	"gitlab.com/erloom-dot-id/go/echo-go-logger/elk"
	"gitlab.com/erloom-dot-id/go/echo-go-logger/sentry"
	"go.uber.org/zap"
)

func (usecase *Usecase) CreateUser(ctx context.Context, payload request.CreateUserRequest) (*model.User, error) {
	ctx, span := sentry.StartSpan(ctx, "usecase.CreateUser")
	defer span.Finish()

	encryptedPassword, err := lib.GenerateHashFromString(payload.Password)
	if err != nil {
		elk.LogError(ctx, "Error EncryptPassword", []zap.Field{
			zap.Error(err),
			zap.Strings("tags", []string{"usecase", "user"}),
		}...)

		return nil, lib.ErrorInternalServer
	}

	var user = model.User{
		Username:          payload.Username,
		Email:             payload.Email,
		Phone:             payload.Phone,
		EncryptedPassword: encryptedPassword,
		IsActiveFlag:      true,
		CreatedAt:         time.Now(),
		CreatedBy:         payload.CreatedBy,
		UpdatedAt:         time.Now(),
		UpdatedBy:         payload.CreatedBy,
	}

	res, err := usecase.repo.CreateUser(ctx, user)
	if err != nil {
		return nil, err
	}

	return &res, nil
}

func (usecase *Usecase) GetUsers(ctx context.Context, query request.GetUserQuery) (*response.GetUserResponse, error) {
	ctx, span := sentry.StartSpan(ctx, "usecase.GetUsers")
	defer span.Finish()

	var res response.GetUserResponse

	users, err := usecase.repo.GetUsers(ctx, query)
	if err != nil {
		return nil, err
	}

	total, err := usecase.repo.GetUserTotal(ctx, query)
	if err != nil {
		return nil, err
	}

	res.Users = users
	res.Total = total
	res.Limit = query.Limit
	res.Offset = query.GetOffset()
	res.Page = query.Page

	return &res, nil
}

func (usecase *Usecase) GetUserDetail(ctx context.Context, ID uint) (*model.User, error) {
	ctx, span := sentry.StartSpan(ctx, "usecase.GetUserDetail")
	defer span.Finish()

	user, err := usecase.repo.GetUserByID(ctx, ID)
	if err != nil {
		return nil, err
	}

	return &user, nil
}

func (usecase *Usecase) UpdateUser(ctx context.Context, payload request.UpdateUserRequest) (*model.User, error) {
	ctx, span := sentry.StartSpan(ctx, "usecase.UpdateUser")
	defer span.Finish()

	user, err := usecase.repo.GetUserByID(ctx, payload.UserID)
	if err != nil {
		return nil, err
	}

	encryptedPassword, err := lib.GenerateHashFromString(payload.Password)
	if err != nil {
		elk.LogError(ctx, "Error EncryptPassword", []zap.Field{
			zap.Error(err),
			zap.Strings("tags", []string{"auth", "login"}),
		}...)

		return nil, lib.ErrorInternalServer
	}

	user.Username = payload.Username
	user.Email = payload.Email
	user.Phone = payload.Phone
	user.EncryptedPassword = encryptedPassword
	user.UpdatedAt = time.Now()
	user.UpdatedBy = payload.UpdatedBy

	res, err := usecase.repo.UpdateUser(ctx, user)
	if err != nil {
		return nil, err
	}

	return &res, nil
}

func (usecase *Usecase) DeleteUser(ctx context.Context, ID uint) error {
	ctx, span := sentry.StartSpan(ctx, "usecase.DeleteUser")
	defer span.Finish()

	err := usecase.repo.DeleteUser(ctx, ID)
	if err != nil {
		return err
	}

	return nil
}
