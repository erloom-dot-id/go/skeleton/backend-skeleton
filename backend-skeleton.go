package backendskeleton

import (
	"backend-skeleton/lib"
	"backend-skeleton/repository"
	"backend-skeleton/storage"
	"backend-skeleton/usecase"
)

type BackendSkeleton struct {
	Usecase *usecase.Usecase
}

func NewBackendSkeleton(db *lib.Database, storage storage.Storage, smtpclient *lib.SMTPClient) BackendSkeleton {
	repository := repository.NewRepository(db, smtpclient)
	usecase := usecase.NewUsecase(&repository, storage)

	return BackendSkeleton{
		Usecase: &usecase,
	}
}
