package lib

import (
	"context"
	"encoding/json"
	"io"
	"log"
	"net/http"
	"os"

	"github.com/growthbook/growthbook-golang"
	"gitlab.com/erloom-dot-id/go/echo-go-logger/elk"
)

// Features API response
type GrowthBookApiResp struct {
	Features json.RawMessage
	Status   int
}

func getFeatureMap() []byte {
	// Fetch features JSON from api
	resp, err := http.Get(os.Getenv("GROWTHBOOK_ENDPOINT"))
	if err != nil {
		return nil
	}
	defer resp.Body.Close()

	body, _ := io.ReadAll(resp.Body)
	// Just return the features map from the API response
	apiResp := &GrowthBookApiResp{}
	_ = json.Unmarshal(body, apiResp)
	return apiResp.Features
}

func NewGrowthbook() *growthbook.GrowthBook {
	featureMap := getFeatureMap()
	if featureMap == nil {
		elk.LogInfo(context.Background(), "Growthbook unconnected")
		return nil
	}
	features := growthbook.ParseFeatureMap(featureMap)

	context := growthbook.NewContext().
		WithFeatures(features).
		// TODO: Use your real analytics tracking system
		WithTrackingCallback(func(experiment *growthbook.Experiment, result *growthbook.Result) {
			log.Println("Viewed Experiment")
			log.Println("Experiment Id", experiment.Key)
			log.Println("Variation Id", result.VariationID)
		})
	gb := growthbook.New(context)

	return gb
}
