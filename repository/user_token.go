package repository

import (
	"backend-skeleton/lib"
	"backend-skeleton/model"
	"context"

	"gitlab.com/erloom-dot-id/go/echo-go-logger/elk"
	"gitlab.com/erloom-dot-id/go/echo-go-logger/sentry"
	"go.uber.org/zap"
)

func (repo *Repository) CreateUserToken(ctx context.Context, userToken model.UserToken) (model.UserToken, error) {
	ctx, span := sentry.StartSpan(ctx, "repository.CreateUserToken")
	defer span.Finish()

	tx, ok := ctx.Value("Trx").(*lib.Database)
	if !ok {
		tx = repo.db
	}

	err := tx.Create(&userToken).Error
	if err != nil {
		elk.LogError(ctx, "Error CreateUserToken", []zap.Field{
			zap.Error(err),
			zap.Strings("tags", []string{"postgres", "user-token", "repo"}),
		}...)

		return userToken, err
	}

	return userToken, nil
}

func (repo *Repository) UpdateUserToken(ctx context.Context, userToken model.UserToken) (model.UserToken, error) {
	ctx, span := sentry.StartSpan(ctx, "repository.UpdateUserToken")
	defer span.Finish()

	tx, ok := ctx.Value("Trx").(*lib.Database)
	if !ok {
		tx = repo.db
	}

	err := tx.Save(&userToken).Error
	if err != nil {
		elk.LogError(ctx, "Error UpdateUserToken", []zap.Field{
			zap.Error(err),
			zap.Strings("tags", []string{"postgres", "user-token", "repo"}),
		}...)

		return userToken, err
	}

	return userToken, nil
}
