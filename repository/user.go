package repository

import (
	"backend-skeleton/lib"
	"backend-skeleton/model"
	"backend-skeleton/request"
	"context"
	"errors"
	"fmt"

	"gitlab.com/erloom-dot-id/go/echo-go-logger/elk"
	"gitlab.com/erloom-dot-id/go/echo-go-logger/sentry"
	"go.uber.org/zap"
	"gorm.io/gorm"
)

func (repo *Repository) CreateUser(ctx context.Context, user model.User) (model.User, error) {
	ctx, span := sentry.StartSpan(ctx, "repository.CreateUser")
	defer span.Finish()

	tx, ok := ctx.Value("Trx").(*lib.Database)
	if !ok {
		tx = repo.db
	}

	err := tx.Create(&user).Error
	if err != nil {
		elk.LogError(ctx, "Error CreateUser", []zap.Field{
			zap.Error(err),
			zap.Strings("tags", []string{"postgres", "user", "repo"}),
		}...)

		return user, err
	}

	return user, nil
}

func (repo *Repository) GetUsers(ctx context.Context, query request.GetUserQuery) ([]model.User, error) {
	ctx, span := sentry.StartSpan(ctx, "repository.GetUsers")
	defer span.Finish()

	var res []model.User
	statement := repo.db.Model(&res).
		Where("is_active_flag = ?", true)

	if query.Search != "" {
		querySearch := fmt.Sprintf("%s%s%s", "%", query.Search, "%")
		statement = statement.Where("name ILIKE ?", querySearch)
	}

	order := query.GetOrderQuery()
	if order != "" {
		statement = statement.Order(order)
	}

	err := statement.Limit(int(query.Limit)).
		Offset(int(query.GetOffset())).
		Find(&res).Error

	if err != nil {
		elk.LogError(ctx, "Error GetUser", []zap.Field{
			zap.Error(err),
			zap.Strings("tags", []string{"postgres", "user", "repo"}),
		}...)

		return res, err
	}

	return res, nil
}

func (repo *Repository) GetUserTotal(ctx context.Context, query request.GetUserQuery) (uint, error) {
	ctx, span := sentry.StartSpan(ctx, "repository.GetUserTotal")
	defer span.Finish()

	var total int64
	var user model.User
	statement := repo.db.Model(&user).
		Where("is_active_flag = ?", true)

	if query.Search != "" {
		querySearch := fmt.Sprintf("%s%s%s", "%", query.Search, "%")
		statement = statement.Where("name ILIKE ?", querySearch)
	}

	order := query.GetOrderQuery()
	if order != "" {
		statement = statement.Order(order)
	}

	err := statement.Count(&total).Error

	if err != nil {
		elk.LogError(ctx, "Error GetUserTotal", []zap.Field{
			zap.Error(err),
			zap.Strings("tags", []string{"postgres", "user", "repo"}),
		}...)

		return uint(total), err
	}

	return uint(total), nil
}

func (repo *Repository) UpdateUser(ctx context.Context, user model.User) (model.User, error) {
	ctx, span := sentry.StartSpan(ctx, "repository.UpdateUser")
	defer span.Finish()

	tx, ok := ctx.Value("Trx").(*lib.Database)
	if !ok {
		tx = repo.db
	}

	err := tx.Save(&user).Error
	if err != nil {
		elk.LogError(ctx, "Error UpdateUser", []zap.Field{
			zap.Error(err),
			zap.Strings("tags", []string{"postgres", "user", "repo"}),
		}...)

		return user, err
	}

	return user, nil
}

func (repo *Repository) GetUserByID(ctx context.Context, ID uint) (user model.User, err error) {
	ctx, span := sentry.StartSpan(ctx, "repository.GetUserByID")
	defer span.Finish()

	tx, ok := ctx.Value("Trx").(*lib.Database)
	if !ok {
		tx = repo.db
	}

	err = tx.First(&user, ID).Error
	if err != nil {
		elk.LogError(ctx, "Error GetUserByID", []zap.Field{
			zap.Error(err),
			zap.Strings("tags", []string{"postgres", "user", "repo"}),
		}...)

		return user, err
	}

	if errors.Is(err, gorm.ErrRecordNotFound) {
		return user, lib.ErrorNotFound
	}

	return user, nil
}

func (repo *Repository) DeleteUser(ctx context.Context, ID uint) error {
	ctx, span := sentry.StartSpan(ctx, "repository.DeleteUser")
	defer span.Finish()

	tx, ok := ctx.Value("Trx").(*lib.Database)
	if !ok {
		tx = repo.db
	}

	var user model.User
	err := tx.Delete(&user, ID).Error
	if err != nil {
		elk.LogError(ctx, "Error DeleteUser", []zap.Field{
			zap.Error(err),
			zap.Strings("tags", []string{"postgres", "user", "repo"}),
		}...)

		return err
	}

	return nil
}

func (repo *Repository) GetUserByUsername(ctx context.Context, username string) (user model.User, err error) {
	ctx, span := sentry.StartSpan(ctx, "repository.GetUserByUsername")
	defer span.Finish()

	tx, ok := ctx.Value("Trx").(*lib.Database)
	if !ok {
		tx = repo.db
	}

	err = tx.Where("username", username).First(&user).Error
	if err != nil {
		elk.LogError(ctx, "Error GetUserByUsername", []zap.Field{
			zap.Error(err),
			zap.Strings("tags", []string{"postgres", "user", "repo"}),
		}...)

		return user, err
	}

	if errors.Is(err, gorm.ErrRecordNotFound) {
		return user, lib.ErrorNotFound
	}

	return user, nil
}
