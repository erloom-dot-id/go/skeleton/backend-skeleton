package repository

import (
	"backend-skeleton/lib"
	"context"
	"errors"

	"gitlab.com/erloom-dot-id/go/echo-go-logger/elk"
	"go.uber.org/zap"
	"gorm.io/gorm"
)

type Repository struct {
	db         *lib.Database
	smtpClient *lib.SMTPClient
}

func NewRepository(db *lib.Database, smtpClient *lib.SMTPClient) Repository {
	return Repository{db: db, smtpClient: smtpClient}
}

func (repo *Repository) Transaction(ctx context.Context, fn func(context.Context) error) error {
	trx := repo.db.Begin()

	ctx = context.WithValue(ctx, "Trx", &lib.Database{DB: trx})
	if err := fn(ctx); err != nil {
		trx.Rollback()
		return err
	}

	return trx.Commit().Error
}

func (repo *Repository) SendEmail(ctx context.Context, req lib.SMTPRequest) error {
	return repo.smtpClient.Send(ctx, req)
}

func IsNotFound(err error) bool {
	return errors.Is(err, gorm.ErrRecordNotFound)
}

func LogError(ctx context.Context, message string, err error) {
	elk.LogError(ctx, message, []zap.Field{
		zap.Error(err),
		zap.Strings("tags", []string{"gorm"}),
	}...)
}

func LogWarn(ctx context.Context, message string) {
	elk.LogWarn(ctx, message, []zap.Field{
		zap.Strings("tags", []string{"gorm"}),
	}...)
}
