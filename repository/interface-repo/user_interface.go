package interfacerepo

import (
	"backend-skeleton/model"
	"backend-skeleton/request"
	"context"
)

type UserRepository interface {
	CreateUser(ctx context.Context, user model.User) (model.User, error)
	GetUsers(ctx context.Context, query request.GetUserQuery) ([]model.User, error)
	GetUserTotal(ctx context.Context, query request.GetUserQuery) (uint, error)
	UpdateUser(ctx context.Context, user model.User) (model.User, error)
	GetUserByID(ctx context.Context, ID uint) (model.User, error)
	DeleteUser(ctx context.Context, ID uint) error
	GetUserByUsername(ctx context.Context, username string) (model.User, error)
}
