package interfacerepo

import (
	"backend-skeleton/model"
	"context"
)

type UserTokenInterface interface {
	CreateUserToken(context.Context, model.UserToken) (model.UserToken, error)
	UpdateUserToken(context.Context, model.UserToken) (model.UserToken, error)
}
