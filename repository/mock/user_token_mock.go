package mock

import (
	"backend-skeleton/model"
	"context"
)

func (m *MockRepository) CreateUserToken(ctx context.Context, userToken model.UserToken) (model.UserToken, error) {
	arguments := m.Called(userToken)
	res := arguments.Get(0).(model.UserToken)

	if arguments.Error(1) != nil {
		return model.UserToken{}, arguments.Error(1)
	}

	return res, nil
}

func (m *MockRepository) UpdateUserToken(ctx context.Context, userToken model.UserToken) (model.UserToken, error) {
	arguments := m.Called(userToken)

	if arguments.Error(1) != nil {
		return model.UserToken{}, arguments.Error(1)
	} else {
		res := arguments.Get(0).(model.UserToken)
		return res, nil
	}
}
