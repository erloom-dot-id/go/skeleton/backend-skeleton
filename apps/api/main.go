package main

import (
	backendskeleton "backend-skeleton"
	"backend-skeleton/config"
	"backend-skeleton/handler"
	"net/http"
	"time"

	echogomiddleware "gitlab.com/erloom-dot-id/go/echo-go-middleware"

	"github.com/go-chi/chi/v5"
	"github.com/joho/godotenv"
)

func init() {
	godotenv.Load()
	config.NewSentry()
	config.NewLogger()
}

func main() {
	db, _ := config.NewPG()
	storage := config.NewStorage()
	smtpClient := config.NewSMTPClient()

	backendSkeleton := backendskeleton.NewBackendSkeleton(db, storage, &smtpClient)
	handler := handler.NewHandler(&backendSkeleton)
	loc, _ := time.LoadLocation("Asia/Jakarta")
	time.Local = loc

	router := chi.NewRouter()

	router.Get("/healthz", handler.Healthz)
	router.Get("/api-doc", handler.GetApiDoc)

	router.Group(func(r chi.Router) {
		r.Use(echogomiddleware.InstrumentMiddlewareV1)

		r.Post("/login", handler.Login)
		r.Route("/user", func(r chi.Router) {
			r.Post("/", handler.CreateUser)
			r.Get("/", handler.GetUsers)
			r.Get("/{ID}", handler.GetUserDetail)
			r.Put("/{ID}", handler.UpdateUser)
			r.Delete("/{ID}", handler.DeleteUser)
		})
	})

	server := &http.Server{
		Addr:    "0.0.0.0:8080",
		Handler: router,
	}

	server.ListenAndServe()
}
