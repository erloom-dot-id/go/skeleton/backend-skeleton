package handler

import (
	backendskeleton "backend-skeleton"
	"backend-skeleton/lib"
	"encoding/json"
	"fmt"
	"html/template"
	"net/http"

	"gitlab.com/erloom-dot-id/go/echo-go-logger/elk"
)

type Handler struct {
	BackendSkeleton *backendskeleton.BackendSkeleton
}

func NewHandler(backendskeleton *backendskeleton.BackendSkeleton) Handler {
	return Handler{
		BackendSkeleton: backendskeleton,
	}
}

func (handler *Handler) Healthz(rw http.ResponseWriter, r *http.Request) {
	rw.Header().Set("Content-Type", "application/json")
	elk.LogInfo(r.Context(), "test from skeleton")
	fmt.Fprintln(rw, "server is ok")
}

func (handler *Handler) GetApiDoc(rw http.ResponseWriter, r *http.Request) {
	t, err := template.ParseFiles("public/api/index.html")
	if err != nil {
		WriteError(rw, err)
		return
	}

	t.Execute(rw, nil)
}

type ResponseBody struct {
	Data    interface{}  `json:"data,omitempty"`
	Message string       `json:"message,omitempty"`
	Meta    ResponseMeta `json:"meta"`
}

type ResponseMeta struct {
	HTTPStatus int   `json:"http_status"`
	Total      *uint `json:"total,omitempty"`
	Offset     *uint `json:"offset,omitempty"`
	Limit      *uint `json:"limit,omitempty"`
	Page       *uint `json:"page,omitempty"`
}

type ErrorInfo struct {
	Message string `json:"message"`
	Code    int    `json:"code"`
	Field   string `json:"field,omitempty"`
}

type ErrorBody struct {
	Errors []ErrorInfo `json:"errors"`
	Meta   interface{} `json:"meta"`
}

func WriteError(w http.ResponseWriter, err error) {
	var resp interface{}
	code := http.StatusInternalServerError

	switch errOrig := err.(type) {
	case lib.CustomError:
		resp = ErrorBody{
			Errors: []ErrorInfo{
				{
					Message: errOrig.Message,
					Code:    errOrig.Code,
					Field:   errOrig.Field,
				},
			},
			Meta: ResponseMeta{
				HTTPStatus: errOrig.HTTPCode,
			},
		}

		code = errOrig.HTTPCode
	default:
		resp = ResponseBody{
			Message: "Internal Server Error",
			Meta: ResponseMeta{
				HTTPStatus: code,
			},
		}
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	json.NewEncoder(w).Encode(resp)
}

func WriteSuccess(w http.ResponseWriter, data interface{}, message string, meta ResponseMeta) {
	resp := ResponseBody{
		Message: message,
		Data:    data,
		Meta:    meta,
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(meta.HTTPStatus)
	json.NewEncoder(w).Encode(resp)
}

func WriteResponse(w http.ResponseWriter, resp interface{}, status int) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	json.NewEncoder(w).Encode(resp)
}
