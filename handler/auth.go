package handler

import (
	"backend-skeleton/lib"
	"backend-skeleton/request"
	"encoding/json"
	"net/http"

	"github.com/go-playground/validator/v10"
	"gitlab.com/erloom-dot-id/go/echo-go-logger/sentry"
)

func (handler *Handler) Login(rw http.ResponseWriter, r *http.Request) {
	ctx, span := sentry.StartSpan(r.Context(), "handler.Login")
	defer span.Finish()

	var payload request.LoginRequest

	err := json.NewDecoder(r.Body).Decode(&payload)
	if err != nil {
		WriteError(rw, lib.ErrorInvalidParameter)
		return
	}

	err = validator.New().Struct(payload)
	if err != nil {
		WriteError(rw, lib.ErrorInvalidParameter)
		return
	}

	res, err := handler.BackendSkeleton.Usecase.Login(ctx, payload)
	if err != nil {
		WriteError(rw, err)
		return
	}

	WriteSuccess(rw, res, "success", ResponseMeta{HTTPStatus: http.StatusOK})
}
