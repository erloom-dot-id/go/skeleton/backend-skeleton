package handler

import (
	"backend-skeleton/lib"
	"backend-skeleton/request"
	"encoding/json"
	"net/http"
	"strconv"
	"strings"

	"github.com/go-chi/chi"
	"github.com/go-playground/validator/v10"
	"gitlab.com/erloom-dot-id/go/echo-go-logger/sentry"
)

func (handler *Handler) CreateUser(rw http.ResponseWriter, r *http.Request) {
	ctx, span := sentry.StartSpan(r.Context(), "handler.CreateUser")
	defer span.Finish()

	var payload request.CreateUserRequest

	err := json.NewDecoder(r.Body).Decode(&payload)
	if err != nil {
		WriteError(rw, lib.ErrorInvalidParameter)
		return
	}

	err = validator.New().Struct(payload)
	if err != nil {
		WriteError(rw, lib.ErrorInvalidParameter)
		return
	}

	res, err := handler.BackendSkeleton.Usecase.CreateUser(ctx, payload)
	if err != nil {
		WriteError(rw, err)
		return
	}

	WriteSuccess(rw, res, "success", ResponseMeta{HTTPStatus: http.StatusOK})
}

func (handler *Handler) GetUsers(rw http.ResponseWriter, r *http.Request) {
	gb := lib.NewGrowthbook()
	if gb != nil && gb.Feature("get-user").Off {
		sentry.PutError(r.Context(), lib.ErrorNotFound)
		WriteError(rw, lib.ErrorNotFound)
		return
	}

	ctx, span := sentry.StartSpan(r.Context(), "handler.GetUsers")
	defer span.Finish()

	var limit, page int
	var query request.GetUserQuery
	queryParams := r.URL.Query()
	limit, _ = strconv.Atoi(queryParams.Get("limit"))
	page, _ = strconv.Atoi(queryParams.Get("page"))
	sort := strings.Split(queryParams.Get("sort"), ",")

	query.Limit = uint(limit)
	query.Page = uint(page)
	query.Search = queryParams.Get("search")
	query.Sort = sort

	res, err := handler.BackendSkeleton.Usecase.GetUsers(ctx, query)
	if err != nil {
		WriteError(rw, err)
		return
	}
	WriteSuccess(rw, res, "success", ResponseMeta{HTTPStatus: http.StatusOK})
}

func (handler *Handler) GetUserDetail(rw http.ResponseWriter, r *http.Request) {
	gb := lib.NewGrowthbook()
	if gb != nil && gb.Feature("detail-user").Off {
		sentry.PutError(r.Context(), lib.ErrorNotFound)
		WriteError(rw, lib.ErrorNotFound)
		return
	}

	ctx, span := sentry.StartSpan(r.Context(), "handler.GetUserDetail")
	defer span.Finish()

	queryParams := r.URL.Query()
	paramID, _ := strconv.Atoi(queryParams.Get("id"))

	res, err := handler.BackendSkeleton.Usecase.GetUserDetail(ctx, uint(paramID))
	if err != nil {
		WriteError(rw, err)
		return
	}

	WriteSuccess(rw, res, "success", ResponseMeta{HTTPStatus: http.StatusOK})
}

func (handler *Handler) UpdateUser(rw http.ResponseWriter, r *http.Request) {
	ctx, span := sentry.StartSpan(r.Context(), "handler.UpdateUser")
	defer span.Finish()

	var payload request.UpdateUserRequest

	err := json.NewDecoder(r.Body).Decode(&payload)
	if err != nil {
		WriteError(rw, lib.ErrorInvalidParameter)
		return
	}

	err = validator.New().Struct(payload)
	if err != nil {
		WriteError(rw, lib.ErrorInvalidParameter)
		return
	}

	idString := chi.URLParam(r, "ID")
	id, _ := strconv.ParseUint(idString, 10, 32)

	payload.UserID = uint(id)

	res, err := handler.BackendSkeleton.Usecase.UpdateUser(ctx, payload)
	if err != nil {
		WriteError(rw, err)
		return
	}

	WriteSuccess(rw, res, "success", ResponseMeta{HTTPStatus: http.StatusOK})
}

func (handler *Handler) DeleteUser(rw http.ResponseWriter, r *http.Request) {
	gb := lib.NewGrowthbook()
	if gb != nil && gb.Feature("delete-user").Off {
		sentry.PutError(r.Context(), lib.ErrorNotFound)
		WriteError(rw, lib.ErrorNotFound)
		return
	}

	ctx, span := sentry.StartSpan(r.Context(), "handler.DeleteUser")
	defer span.Finish()

	idString := chi.URLParam(r, "ID")
	id, _ := strconv.ParseUint(idString, 10, 32)

	err := handler.BackendSkeleton.Usecase.DeleteUser(ctx, uint(id))
	if err != nil {
		WriteError(rw, err)
		return
	}

	WriteSuccess(rw, nil, "success", ResponseMeta{HTTPStatus: http.StatusOK})
}
