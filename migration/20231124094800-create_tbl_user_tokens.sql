-- +migrate Up
CREATE TABLE IF NOT EXISTS user_tokens(
    id SERIAL NOT NULL PRIMARY KEY,
    user_id INT NOT NULL,
    token VARCHAR(255) NOT NULL,
    expired_at TIMESTAMP NOT NULL,
    is_active_flag BOOLEAN,
    created_at TIMESTAMP NOT NULL,
    updated_at TIMESTAMP NOT NULL
);

-- +migrate Down
DROP TABLE IF EXISTS user_tokens;